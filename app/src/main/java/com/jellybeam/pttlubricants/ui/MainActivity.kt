package com.jellybeam.pttlubricants.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.jellybeam.pttlubricants.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
